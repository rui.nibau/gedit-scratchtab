# Gedit Scratchtab

Plugin pour Gedit ≥ 3.12 qui permet de placer une feuille de brouillon dans le panneau latéral ou le panneau inférieur afin d'avoir une zone où stocker des notes, des morceaux de textes ou de simples listes de tâches à effectuer.

* Download : https://omacronides.com/lab/gedit-scratchtab/build/latest.tar.gz
* Documentation : https://omacronides.com/p/gedit-scratchtab
* Source : https://framagit.org/rui.nibau/gedit-scratchtab
* Issues : https://framagit.org/rui.nibau/gedit-scratchtab/-/issues
