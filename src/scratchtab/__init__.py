#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import os

from gi.repository import GLib, GObject, Gtk, Gedit, PeasGtk, Tepl
from gi.repository.Gdk import RGBA
from gi.repository.Pango import font_description_from_string

from .config import Config
from .signals import Signals

APP_NAME = 'scratchtab'
FILE_DIR = os.path.dirname(__file__)
GEDIT_CONFIG_DIR = os.path.join(GLib.get_user_config_dir(), 'gedit')

# scratchtab > 3.9
SCRATCH_FILE = os.path.join(GEDIT_CONFIG_DIR, 'scratchtab.txt')
# scratchtab 3.9
DATA_SCRATCH_DIR = os.path.join(GLib.get_user_data_dir(), 'gedit-scratchtab')
DATA_SCRATCH_FILE = os.path.join(DATA_SCRATCH_DIR, 'scratchtab.txt')
# scratchtab < 3.9
LOCAL_SCRATCH_FILE = os.path.join(FILE_DIR, 'scratchtab.txt')

# translation
#utils.localize(APP_NAME, os.path.join(FILE_DIR, 'data', 'locale'))

logger = logging.getLogger(APP_NAME)
logger.setLevel(logging.DEBUG)

class ScratchTabAppActivatable(GObject.Object, Gedit.AppActivatable):
    __gtype_name__ = 'ScratchTabAppActivatable'
    app = GObject.property(type=Gedit.App)

    def do_activate(self):
        ScratchTabWindowActivatable.config = Config({
            'schema_name': 'org.gnome.gedit.plugins.scratchtab',
            'schema_dir' : os.path.join(FILE_DIR, 'data', 'schema'),
            'ui_path'    : os.path.join(FILE_DIR, 'data', 'ui', 'config.glade'),
            'domain'     : APP_NAME,
            'connect'    : True
        })

    def do_deactivate(self):
        ScratchTabWindowActivatable.config = None


class ScratchTabWindowActivatable(GObject.Object,
                                Gedit.WindowActivatable,
                                PeasGtk.Configurable,
                                Signals):

    __gtype_name__ = 'ScratchTabWindowActivatable'
    window = GObject.property(type=Gedit.Window)
    config = None

    def __init__(self):
        GObject.Object.__init__(self)
        Signals.__init__(self)
        assert self.config
        self._panel_item = None
        self._pane = None
        self._textview = None
        self._doc = None
        self._position = None

    def do_create_configure_widget(self):
        '''Create configuration widget'''
        return self.config.create_widget()

    def do_activate(self):
        '''Activate plugin'''
        self._position = self.config.settings.get_string('position')

        # Connect to settings change
        self.config.settings.connect('changed::position', self.on_position_changed)
        self.config.settings.connect('changed::background', self.on_custom_changed)
        self.config.settings.connect('changed::foreground', self.on_custom_changed)
        self.config.settings.connect('changed::font', self.on_custom_changed)

        # créer le panneau
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(FILE_DIR, 'data', 'ui', 'pane.glade'), ['pane'])
        builder.connect_signals(self)

        self._pane = builder.get_object('pane')
        self._textview = builder.get_object('textview')
        self._customize_textview()
        self._doc = self._textview.get_buffer()
        self._append_text(self._load())
        self._add_pane()

        # Connect to window events
        self._connect(self.window, 'tab-added', self.on_tab_added)
        self._connect(self.window, 'tab-removed', self.on_tab_removed)

    def do_deactivate(self):
        '''Deactivate plugin'''
        self._save()
        self._disconnect_all()
        self._remove_pane()
        self._pane = None
        self._doc = None
        self._textview = None

    def on_position_changed(self, settings, key, data=None):
        '''Callback when position changed'''
        self._remove_pane()
        self._position = settings.get_string('position')
        self._add_pane()

    def on_custom_changed(self, settings, key, data=None):
        '''Callback when custom changed'''
        self._customize_textview()

    def on_tab_added(self, window, tab):
        '''Callback when a tab is added'''
        self._connect(tab.get_view(), 'populate-popup', self.on_populate_popup)

    def on_tab_removed(self, window, tab):
        '''Callback when a tab is removed'''
        self._disconnect(tab.get_view())

    def on_textview_focus_out_event(self, widget, event, data=None):
        '''Callback when text loose focus'''
        self._save()

    def on_populate_popup(self, view, menu):
        '''Ecouteur d'ouverture de menu contextuel'''
        doc = view.get_buffer()
        if doc.get_has_selection():
            item = self._get_popup_item()
            item.show()
            menu.prepend(item)

    def on_popup_item_activate(self, item, data=None):
        view = self.window.get_active_view()
        doc = view.get_buffer()
        sel = doc.get_selection_bounds()
        text = doc.get_text(sel[0], sel[1], True)
        self._append_text(text)

    def _add_pane(self):
        panel = getattr(self.window, 'get_' + self._position + '_panel')()
        if isinstance(panel, Tepl.Panel):
            Tepl.Panel.add(panel, self._pane, 'ScratchTab', 'ScratchTab', None)
            try:
                # for Gedit >= 48.1
                self._panel_item = Tepl.PanelItem.new(self._pane, 'ScratchTab', 'ScratchTab', None, 0)
                panel.add(self._panel_item)
            except Exception:
                self._panel_item = None
                Tepl.Panel.add(panel, self._pane, 'ScratchTab', 'ScratchTab', None)
        else:
            panel.add_titled(self._pane, 'ScratchTab', 'ScratchTab')

    def _remove_pane(self):
        panel = getattr(self.window, 'get_' + self._position + '_panel')()
        if self._panel_item is None:
            panel.remove(self._pane)
        else:
            panel.remove(self._panel_item)
            self._panel_item = None

    def _get_popup_item(self):
        item = Gtk.ImageMenuItem(_('send to ScratchTab'))
        item.set_image(Gtk.Image.new_from_stock(Gtk.STOCK_EDIT, Gtk.IconSize.MENU))
        item.connect('activate', self.on_popup_item_activate)
        return item

    def _customize_textview(self):
        '''Customize textview styles'''

        # text color
        value = self.config.settings.get_string('foreground')
        if value:
            rgba = RGBA()
            if rgba.parse(value):
                self._textview.override_color(Gtk.StateType.NORMAL, rgba)

        # background color
        value = self.config.settings.get_string('background')
        if value:
            rgba = RGBA()
            if rgba.parse(value):
                self._textview.override_background_color(Gtk.StateType.NORMAL, rgba)

        # font
        value = self.config.settings.get_string('font')
        if value:
            logger.debug(value)
            font_description = font_description_from_string(value)
            self._textview.override_font(font_description)

    def _append_text(self, text):
        self._doc.insert(self._doc.get_end_iter(), text)
        self._save()

    def _load(self):
        text = ''

        if os.path.isfile(LOCAL_SCRATCH_FILE):
            # old local file (scratchtab < 3.9)
            logger.info('Get content from old scratch file')
            fo = open(LOCAL_SCRATCH_FILE, 'r', encoding="utf-8")
            text = text + fo.read()
            fo.close()
            logger.info('Deleting old scratch file')
            os.remove(LOCAL_SCRATCH_FILE)
        elif os.path.isfile(DATA_SCRATCH_FILE):
            # data dir scratch file (scratchtab 3.9)
            logger.info('Get content from data scratch file')
            fo = open(DATA_SCRATCH_FILE, 'r', encoding="utf-8")
            text = text + fo.read()
            fo.close()
            logger.info('Deleting data scratch file')
            os.remove(DATA_SCRATCH_FILE)
            logger.info('Deleting data scratch dir')
            os.rmdir(DATA_SCRATCH_DIR)

        # file in config dir (scratchtab > 3.9)
        if os.path.isfile(SCRATCH_FILE) is False:
            try:
                os.makedirs(GEDIT_CONFIG_DIR, True)
            except OSError:
                pass
            fo = open(SCRATCH_FILE, 'w+', encoding="utf-8")
            fo.write('')
            fo.close()
        else:
            fo = open(SCRATCH_FILE, 'r', encoding="utf-8")
            text = text + fo.read()
            fo.close()

        return text

    def _save(self):
        start = self._doc.get_start_iter()
        end = self._doc.get_end_iter()
        f = open(SCRATCH_FILE, 'w', encoding="utf-8")
        f.write(self._doc.get_text(start, end, True))
        f.close()
