---
title:      Historique
date:       2007-02-25
updated:    2024-12-21
---

°°changelog°°
2024-12-21:
    • fix: Gedit 48.1 compatibility
2024-05-01:
    • fix: [tepl 6 compatibility](https://gedit-technology.github.io/developer-docs/libgedit-tepl-6/api-breaks-during-tepl-6.html)
2023-12-03:
    • fix: Gedit 46 compatibility.
2015-05-23:
    * fix: Regression dans Gedit 3.12 à cause des fontes monospace. Retour à l'état antérieur.
2015-05-03:
    * add: Personnalisation de la feuille de brouillon (fonte, texte, etc.).
    * upd: Versionning par date de sortie.
3.12 (2014-04-27):
    * upd: Adaptation à Gedit 3.12.
    * upd: Fichier de brouillon dans ~/.config/gedit.
    * del: Suppression du module paths (utilisation de GLib).
3.9 (2014-03-25):
    * upd: Dépendances par fichier et plus par module.
    * upd: Utilisation du nouvel objet de configuration.
    * fix: Fichier de brouillon dans ``.local/share/gedit/plugins/scratchtab/``.
3.8 (2013-07-06):
    * upd: Adaptation du plugin à Gedit 3.8 (python 3).
    * add: organisation standardisée du plugin.
3.6 (2013-05-12):
    * upd: Restructuration du plugin.
    * upd: Utilisation du module GSettings.
3.0 (2012-08-23):
    * upd: Première version pour Gedit 3.
    * add: Intégration du module ``common`` pour tous les plugins Gedit.
    * upd: Numérotation principale identique à celle de Gedit.
    * upd: Réecriture en GTK 3.
    * upd: Internationalisation avec ``gettext``.
1.0 (2009-05-21):
    * upd: Première version considérée comme stable.
0.4 (2009-04-28):
    * upd: Brouillon dans le panneau latéral ou inférieur.
0.3 (2009-04-18):
    * add: Internationalisation.
    * upd: Nettoyage du code et commentaires.
    * upd: Amélioration de la gestion des événements.
0.2 (2008-10-25):
    * add: Enregistrer le brouillon dés que l'utilisateur quitte l'onglet ScratchTab.
    * add: Commande dans le menu contextuel pour copier la sélection dans ScratchTab.
0.1 (2007-02-25):
    * add: Première version utilisée - alpha (python newbie)
