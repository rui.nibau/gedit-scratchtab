---
title:      Gedit : ScratchTab
banner:     /lab/gedit-scratchtab/pics/2021-10-28.png
date:       2007-02-25
updated:    2024-12-21
cats:       [informatique]
tags:       [gedit, plugin, scratchtab]
techs:      [python, gtk]
status:     in-process
version:    2024-12-21
source:     https://framagit.org/rui.nibau/gedit-scratchtab
issues:     https://framagit.org/rui.nibau/gedit-scratchtab/-/issues
download:   /lab/gedit-scratchtab/build/latest.zip
contact:    true
itemtype:   SoftwareApplication
pagesAsSnippets: true
intro: ''ScratchTab'' est un plugin pour Gedit ≥ 3.12 qui permet de placer une feuille de brouillon dans le panneau latéral ou le panneau inférieur afin d'avoir une zone où stocker des notes, des morceaux de textes ou de simples listes de tâches à effectuer.
---

°°|install°°
## Installation

..include:: /data/snippets/gedit-plugin-install.md
    plugin-name = scratchtab


°°|usage°°
## Utilisation

°°pic pos-centre sz-full°°
----
![Image](/lab/gedit-scratchtab/pics/2015-05-03.png)
----
ScratchTab ouvert dans le panneau latéral de Gedit avec le menu contextuel lorsque du texte est sélectionné.

ScratchTab peut être utilisé comme une zone de prise de notes en plus de la zone d'édition des fichiers de Gedit ou alors comme feuille de brouillon dans laquelle on peut envoyer du texte sélectionné dans le fichier courant.


°°|config°°
## Configuration

°°pic pos-left sz-half°°
----
![Capture d'écran](/lab/gedit-scratchtab/pics/2015-05-03-config.png)
----
Fenêtre de configuration.

• ``position`` : dans quel panneau ScratchTab doit s'ouvrir : latéral (``side``) ou inférieur (``bottom``).
• ``foreground`` : la couleur du texte.
• ``background`` : la couleur de fond.
• ``font`` : La police de caractères.

°°|issues°°
## Bugs et évolutions

...

°°|history°°
## Historique

..include:: ./changelog.md

°°|res-ref°°
## Ressources et références

..include:: /data/snippets/gedit-plugin-ref.md

°°|licence°°
## Licence

..include:: ./licence.md

