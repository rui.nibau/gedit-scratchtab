#!/bin/bash

# ==============================================================================
# Script d'installation de scratchtab
# ==============================================================================

CURRENT_DIR="$( cd "$( dirname "$0" )" && pwd )"
PLUGIN_DIR=~/.local/share/gedit/plugins
PLUGIN_NAME="scratchtab"

# Install
install() {
    mkdir -p $PLUGIN_DIR
    cp -pr $CURRENT_DIR$PLUGIN_NAME $CURRENT_DIR$PLUGIN_NAME".plugin" $PLUGIN_DIR
    echo $PLUGIN_NAME" installed."
}

# Uninstall
uninstall() {
    rm -fr $PLUGIN_DIR"/"$PLUGIN_NAME $PLUGIN_DIR"/"$PLUGIN_NAME".plugin"
    echo $PLUGIN_NAME" uninstalled."
}

# Helper
helper() {
    local bold=`tput bold`
    local underline=`tput smul`
    local normal=`tput sgr0`
    echo -e "Script to manage Gedit plugin: scratchtab\n"
    echo -e "${bold}USAGE"
    echo -e "\tplugin.sh install"
    echo -e "${normal}\tInstall "$PLUGIN_NAME" Gedit plugin\n"
    echo -e "${bold}\tplugin.sh uninstall"
    echo -e "${normal}\tUninstall "$PLUGIN_NAME" Gedit plugin\n"
}

# Main
case $1 in
    install) install;;
    uninstall) uninstall;;
    *|help) helper;; 
esac

